# Usage #

## Building the Code ##
A makefile has been provided for convenience. To build the code, simply cd from the root of the repository, to src. Once there, type the command "make", and all relevant java files will be compiled.



## Normal Use Case ##
Follow the below instructions to run the program in Normal mode. Normal mode allows a client to connect to a server, then communicate with other clients connected to the server though a GUI.


### First Run A Server ###
A server takes 0 or 1 arguments. The optional argument is the port number the server process will run on. If 0 arguments are provided, the server will launch with default port 1234.

From the src directory type: 
"java ChatServer/ChatServer {optional port number}"


### Run A Client ###
Once the server has been successfully launched, a client can be launched from any other PC on the network to connect to it. To run the client in normal mode, while in the src directory, type:<br>
"java ChatClient/ChatClient [server port number] [server IP address] [desired username]"

If done correctly (and the desired username is not already in use), the client's chat GUI should appear.


### Using the GUI ###
Once the GUI has launched, you can now send messages to any desired username! Simply type the name of the user you want to message in the textbox next to the "To:" field, type your message in the textbox on the bottom of the GUI, then hit the "Enter" key on your keyboard, or hit the "Send" button.

If the user is online, they will receive the message almost immediately (depending on network connection). If the destination user (user you are trying to send a message to) is not online or the server is down, a warning message will appear in the GUI. Messages sent to offline users will be sent when/if they connect to the server.


## Using Analysis Mode ##
Analysis mode allows you to test the average delivery time of messages sent between X clients through a single server. This is done by launching a server, then launching X/2 clients as "Receivers". Receivers simply launch and wait to receive all the messages they are expecting to get, then die. Finally, the remaining X/2 clients should be launched as "Senders". Senders automatically send 3 messages to their "Receiver Pair", then die. The time it took for each message to travel from a Sender to a Receiver is documented by the server, and outputted as the DeliveryTimes.txt file under src/AnalysisResults directory. 

### Launching Senders and Receivers ###
Analysis mode is primarily operated through the use of python scripts. To run in analysis mode, first open a server (as explained in an above section). Then, on any machine, from the src directory, type:<br>
"python LaunchSendersAndReceivers.py [number of Senders/Receivers to launch here] [Server IP Address] [String literal "Sender" or "Receiver"]"<br>
When the output on the terminal stops for both the sender and receiver, you know they are done working. You can hit the "Enter" key on the command line to regain access to the terminal.


### Getting the Results ###
Results from the Senders and Receivers test are stored in AnalysisResults/DeliveryTimes.txt. The average of these results are computed using the python script ParseTestResults.py. The results will be printed to the terminal. To use this script, from the src directory, type:<br>
"python ParseTestResults.py"

<br><br><br><br>


# Requirements #
The following are requirements specified by the lab assignment Client and Server.
We will discuss each requirement and how they were addressed and implemented
in each case.
## Threaded Server ##
The server will accept multiple connections by spawning new threads to handle 
each connection. A connection is created when a client wants to send a message to 
the server. Each connection will be handled by a thread a server creates, the message 
is handled by the server protocol and when it finishes the thread dies. 
Handling each connection by spawning a new thread, allows the server
to handle multiple connections. The server will then wait for additional
connections to be made to spawn more threads to handle each connection. The formal
procedure that the Server follows is to create, execute, and terminate threads.
It creates a new thread when a connection is accepted, handles the connection by 
processing the message through our protocol, and terminates the thread when the
message is processed.
## GUI ##
More specifically, the requirements state that we have a single textbox to allow
users to write messages, and a scrollable text box for the user to view messages.
In our GUI, we provide a textbox to accept input from the user and a text area
which displays the chat history of the client. 

Additionally, we provide a textbox in the upper right corner to specify the username
that the client would like to send a message to. With the mentioned features provided,
the client will have the necessary tools to send another client a message. The user
is able to type a message into the chat text field, which then can be sent using the
submit or enter key. If a proper target username is provided the message will be delivered,
and the chat history will be updated to allow the client to view previous messages.

## Client / Server Has High Availability and Prevents from Crashing ##
The Client maintains a high availability and prevents from crashing assuming it was 
initially was able to connect to a server beforehand. If a client was connected 
beforehand, and the server disconnected unexpectedly, the GUI will not crash. It
would instead inform the user that there is an issue with the server. 

This was handled by checking whether a connection can be established when initially
trying to send a message to the server. If a connection could not be made with a server,
the chat client will be informed and the error message is displayed to the user.

Moreover, the server maintains a high availability and prevents from crashing regardless
of clients unexpectedly disconnecting. Since threads are spawned each time a connection is 
made, the server thread is independent of those threads. If a connection is unexpectedly broken
the thread simply dies and is cleaned by the java compiler. Additionally, there are failure
checks to properly handle the possible failures of connections. The server will continue
to accept more connections and create more threads, regardless of client disconnections.
This was tested by launching the server, and continuously launching and disconnecting clients. 
We observed that the server maintains a high availability and prevents from crashing.

## Messages are Stored if Target is Offline, Deliver When They Connect ##
Messages are always stored at the Server, every time a chat message is received by the 
server, it will store the message into a data structure regardless of whether the client
is online or offline. This is handled by the design of the client and server. The server 
will always store any message received, and clients must request their messages by asking the server.

## Sender / Receiver Mode ##
To handle the graduation requirement, client pairs are made to ensure that one client will be
receiving and another is sending messages. Client pairs are created to record the delivery time that
a message is sent between a client pair. Client pairs are identified by a client either being
in a receiving / sender mode. The sender client will send messages to it's receiving counterpart
and timestamps the time that the message was initially sent. The receiver client will receive messages from it's sender counterpart and timestamps the time that it receives a message. The timestamp data is then sent to the 
server, and the server calculates and stores the delivery time to a file. For example, for 10000 total clients, a 
script is used to start 5000 senders clients on one machine, and 5000 receiving clients are made on another machine. After all the clients have sent and received all of their messages the clients will die. The result is 5000 * number of messages sent by senders delivery times. Lastly the average is computed using a script which
is outputted to the user. 


<br><br><br><br>


# Analysis Results for Graduate Requirement #

## Experiment Details ##
Tests were run for 10, 100, 1000, and 10000 clients. Each Sender sent a total of 3 messages to its "Receiver partner" before both the Receiver and Sender would disconnect from the server. In each test, there were a total of n/2 Receivers and n/2 Senders where n is the number of total clients. The Server and Receivers were launched on one machine, while the Senders were launched on another close-by machine in the CS labs in Farris at UNM (both on the same LAN). Receivers were always launched first. If senders were launched first, the delivery time for a message would include the time it took for a Sender's "Receiver partner" to log on. 

## Experiment Results ##
As the number of clients increased, the average delivery time also increased.


## Data Evaluation ##
We suspect the reason for this behavior lies with the server's design (and how it handles threads). The server has a thread pool that it stores new threads in. Each of these threads is a connection to the server socket. The thread pool only allows 50 concurrent threads to be running at any given time. This is necessary to prevent the system from crashing due to running out of CPU resources. 

Due to this restriction, it seems that the more clients requesting to send messages through the server at once, the longer they will need to wait in the queue to be service, causing an increase in the delivery time.

We have also concluded that delivery time of messages was heavily affected by our limitation of only being able to use 2 PCs for the experiment. Each PC was responsible for running anywhere from 5-5000 clients concurrently. Since the CPUs on these machines have 8 cores, it would be reasonable to assume much of the delivery time "lag" is caused by CPU context switching. 

<br><br><br><br>

# Developer Activities #
The following is a log of the development of the Client Server project. Dates will be listed followed by 
incremental steps that completed a certain goal. The main idea of our approach was that we planned a design 
before implementing the code. We used pair programming to diagnose and implement code

<b> 08/31/2019 </b>
Designed an initial design which was the infrastructure for communication of clients with a server. Concluded
that each client would have a persistent connection between server and client. Clients and Server would have
it's own protocol to handle each others messages. Servers would be able to forward messages to clients and accept
messages from other clients to forward.

<b> 09/09/2019 </b>
Specified and flushed out details of messages that Server and Clients handles. Messages are used
as a means to send information between server and client. Defined additional data structures to 
determine how server will store messages when clients are offline.

<b> 09/11/2019 </b>
Redesigned the relationship between client and server, there was an issue with maintaining persistent
connections with thousands of clients due to limited resources. Connections are now made when a client wants 
to send a message to the server. The server will spawn new threads for multiple connections which could be made by 
multiple clients. Connections are closed out when message is processed by server. This solved the problem
by closing out connections and freeing up resources to allow additional connections to be processed.

<b> 09/14/2019 </b>
Determined there was issue the amount of threads the server was creating. The server was creating
many threads and ended up killing itself. After additional analysis, we decided that a thread pool was 
necessary to manage the maximum amount of threads that can run concurrently. Moreover, implemented GUI to 
allow the user to interact with the chat client, and created python scripts to interpret and analyze data
of delivery times.

<b> 09/15/2019 </b>
Documented, refactored, and cleaned the code. Additionally, constructed the wiki from other documented resources that we created during development. 


