import sys
import subprocess

num_of_client_pairs = sys.argv[1]
server_host = sys.argv[2]
mode = sys.argv[3]


if mode == 'Receiver':

    for i in range(int(num_of_client_pairs)):
        receiver_name = 'Receiver' + str(i)
        java_command = 'java ' + 'ChatClient/ChatClient' + ' 1234 ' + server_host + ' ' + receiver_name + ' Receiver'
        subprocess.Popen(java_command.split())

elif mode == 'Sender':

    for i in range(int(num_of_client_pairs)):
        receiver_name = 'Receiver' + str(i)
        sender_name = 'Sender' + str(i)
        java_command = 'java ChatClient/ChatClient 1234 ' + server_host \
                       + ' ' + sender_name + ' Sender ' + receiver_name
        subprocess.Popen(java_command.split())

exit(0)
