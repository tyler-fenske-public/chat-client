NUM_OF_MESSAGES_SENT_PER_SENDING_CLIENT = 3
data_file = open("AnalysisResults/DeliveryTimes.txt")
parsed_data_file = data_file.readlines()

number_of_results = len(parsed_data_file)
num_of_clients = number_of_results / NUM_OF_MESSAGES_SENT_PER_SENDING_CLIENT * 2
sum_of_delivery_times = 0

for x in parsed_data_file:
    sum_of_delivery_times += int(x)

average_delivery_time = sum_of_delivery_times / number_of_results

print("Average Delivery Time for " + str(num_of_clients) + " Clients is " + str(average_delivery_time) + " milliseconds")