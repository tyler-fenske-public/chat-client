package Utility;

import java.io.Serializable;

/**
 * MessageContent is used as a serialized container class to be sent in messages over the network. This container
 * holds the data needed to be passed between clients and servers.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class MessageContent implements Serializable {

    private String sourceUser = "";
    private String destinationUser = "";
    private String chatContent = "";
    private String timestampSent = "";
    private String timestampReceived = "";

    /**
     * @return the Source User's username.
     */
    public String getSourceUser() {
        return sourceUser;
    }

    /**
     * Sets the Source User's username.
     * @param sourceUser the username of the client that created the message.
     */
    public void setSourceUser(String sourceUser) {
        this.sourceUser = sourceUser;
    }

    /**
     * @return the Destination User's username.
     */
    public String getDestinationUser() {
        return destinationUser;
    }

    /**
     * Sets the Destination User's username.
     * @param destinationUser the username of the client that the message was intended to be sent to.
     */
    public void setDestinationUser(String destinationUser) {
        this.destinationUser = destinationUser;
    }

    /**
     * @return the contents of a chat message.
     */
    public String getChatContent() {
        return chatContent;
    }

    /**
     * Sets the contents of the chat message.
     * @param chatContent the chat message.
     */
    public void setChatContent(String chatContent) {
        this.chatContent = chatContent;
    }

    /**
     * @return the timestamp of when the message was sent by the source user.
     */
    public String getTimestampSent() {
        return timestampSent;
    }

    /**
     * Sets the timestamp of when the message was sent by the source user.
     * @param timestampSent a String that represents the time a message was sent in the format HH:MM:SS:LLLL where
     *                      H represents Hours, M represents Minutes, S represents Seconds, and L represents
     *                      milliseconds.
     */
    public void setTimestampSent(String timestampSent) {
        this.timestampSent = timestampSent;
    }

    /**
     * @return the timestamp of when the message was received by the destination user.
     */
    public String getTimestampReceived() {
        return timestampReceived;
    }

    /**
     * Sets the timestamp of when the message was received by the destination user.
     * @param timestampReceived a String that represents the time a message was received in the format HH:MM:SS:LLLL
     *                          where H represents Hours, M represents Minutes, S represents Seconds, and L represents
     *                          milliseconds.
     */
    public void setTimestampReceived(String timestampReceived) {
        this.timestampReceived = timestampReceived;
    }

}
