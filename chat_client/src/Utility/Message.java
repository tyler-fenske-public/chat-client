package Utility;

import java.io.Serializable;

/**
 * Provides the underlying structure of a Message to allow
 * communication between the ChatClient and ChatServer
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class Message implements Serializable {

    public enum MessageIdentifier {
        CONNECTION_REQ,              // From Client to Server Requesting Connection
        CONNECTION_ACCEPT,           // Response From Server to Client Accepting Connection
        USERNAME_IN_USE,             // Response From Server to Client Rejecting Connection
        CHAT_MSG,                    // From A to B Message With Chat Content
        GET_NEXT_QUEUED_MSG,         // From Client to Server Requesting Unread Messages
        DISCONNECT,                  // From Client to Server Requesting Disconnect From Server
        OKAY,                        // Response From Server to Client Notifying Request Processed
        MSG_PENDING,                 // Response From Server to Client if Destination User is Offline
        TIMESTAMP_DATA,              // From Client to Server after Client receives new message
    }

    private MessageIdentifier messageIdentifier;
    private MessageContent messageContent;

    /**
     * Public constructor for a Message, which is an information structure
     * used for communication between the ChatClient and ChatServer.
     * @param messageIdentifier enum identifying the type of message
     * @param messageContent Object containing message information
     */
    public Message (MessageIdentifier messageIdentifier,
                    MessageContent messageContent) {

        this.messageIdentifier = messageIdentifier;
        this.messageContent = messageContent;

    }

    /**
     * Returns the Message's identifier, which specifies the type of message
     * @return MessageIdentifier in the form of a predefined enum.
     */
    public MessageIdentifier getMessageIdentifier() {
        return messageIdentifier;
    }

    /**
     * Returns the Message's actual content in the form of an Object, which
     * can then be cast or interpreted based on the Message's MessageIdentifier
     * @return Object to be typecast representing the Message content.
     */
    public MessageContent getMessageContent() {
        return messageContent;
    }

}
