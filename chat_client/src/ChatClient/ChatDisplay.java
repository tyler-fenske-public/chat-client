package ChatClient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Uses swing to launch the ChatClients gui, will have a chat message
 * textfield, chat history, send button, username label, and target field.
 * Will provide a ChatClient a method to notify the ChatDisplay to updates
 * its gui.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class ChatDisplay {

    private ChatClient chatClient;
    private String username;
    private JTextArea chatLog;
    private JScrollPane scrollPane;

    /**
     * Will setup perform setup and initialize the display.
     * @param chatClient ChatClient that the ChatDisplay will be displaying for.
     * @param username name of the username.
     */
    public ChatDisplay(ChatClient chatClient, String username){
        this.chatClient = chatClient;
        this.username = username;
        initializeDisplay();
    }

    /**
     * Given the username and chat contents will display that onto the users gui.
     * @param username name of the user.
     * @param chatContent contents to be displayed.
     */
    public void displayNewMessage(String username, String chatContent){
        chatLog.append(formatChatMessage(username, chatContent));

    }

    /**
     * Will display a message to the gui to inform the user that the destination user
     * of a message being sent is currently offline.
     * @param username
     */
    public void displayMessagePending(String username){
        chatLog.append(username + " is currently offline. They will receive your message when they log on.\n");

    }

    /**
     * Will display a message to the gui to inform the user that the server is currently
     * offline.
     */
    public void displayServerUnresponsiveMessage(){
        chatLog.append("Message not sent, the server is offline.\n");
    }

    /**
     * Setup for swing components for the gui, adds appropriate action listeners for
     * text fields, and send button.
     */
    private void initializeDisplay(){
        // Sets up main window to be displayed to the user.
        JFrame chatWindow = new JFrame();
        chatWindow.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        chatWindow.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                chatClient.disconnectClient();
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        chatWindow.setTitle("Chat");

        //Send button to send a message to target user.
        JButton sendButton = new JButton("Send");
        sendButton.setBounds(575, 315, 100, 30);

        //Field to allow th user to type in their chat message to another user.
        JTextField chatTextField = new JTextField(0);
        chatTextField.setFont(new Font("Arial", Font.BOLD, 16));
        chatTextField.setBounds(25, 315, 525, 30);

        //Text field to allow the user to type in the target username of any message.
        JTextField toTextField = new JTextField(0);
        toTextField.setFont(new Font("TimesRoman", Font.BOLD, 18));
        toTextField.setBounds(525, 15, 150, 30);

        //Text label to let the user know what the text field is for.
        //To is the target user.
        JLabel to = new JLabel("To:");
        to.setBounds(485, 15, 40, 30);
        to.setFont(new Font("TimesRoman", Font.BOLD, 18));

        //Text label to display the username of the client.
        JLabel usernameLabel = new JLabel("Username: " + username);
        usernameLabel.setBounds(25, 15, 400, 30);
        usernameLabel.setFont(new Font("TimesRoman", Font.BOLD, 18));

        //Chat history, which contains all the previous messages sent and received.
        chatLog = new JTextArea(5, 5);
        chatLog.setLineWrap(true);
        chatLog.setWrapStyleWord(true);
        chatLog.setEditable(false);
        chatLog.setFont(new Font("Arial", Font.BOLD, 16));

        //Scroll bar to allow the user to view the entire chat history.
        scrollPane = new JScrollPane(chatLog);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(25, 60, 650,235);
        scrollPane.getVerticalScrollBar().addAdjustmentListener( e ->
                chatLog.select(chatLog.getHeight() + 1000, 0));

        //Adds a key listener to allow user to press enter to send a chat message,
        //if a message is in the chat text field.
        chatTextField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER){
                    sendChatMessage(chatTextField, toTextField);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        //Action listener for button to allow user to click the send button.
        sendButton.addActionListener(e -> sendChatMessage(chatTextField, toTextField));

        //Adds components to main window.
        chatWindow.add(scrollPane);
        chatWindow.add(sendButton);
        chatWindow.add(chatTextField);
        chatWindow.add(toTextField);
        chatWindow.add(to);
        chatWindow.add(usernameLabel);

        //Adjust configurations for chat window.
        chatWindow.setSize(700,400);
        chatWindow.setLayout(null);
        chatWindow.setVisible(true);
    }

    /**
     * Properly formats chat message to be displayed to gui.
     * @param username name of the user
     * @param messageContent content to be displayed to gui.
     */
    private String formatChatMessage(String username, String messageContent){
        return username + ": " + messageContent + "\n";
    }

    /**
     * Use by the ChatDisplay to send messages, given proper field that were extracted
     * from the gui. Send extracted text to the chat client to be sent to the server.
     * This is done by utilizing ChatClient ability to send messages to the server.
     * @param chatTextField text field containing contents of the chat.
     * @param toTextField target username for contents to be sent to.
     */
    private void sendChatMessage(JTextField chatTextField, JTextField toTextField){
        String textFieldMessage = chatTextField.getText();
        String destinationUser = toTextField.getText();

        if(!textFieldMessage.equals("") && !destinationUser.equals("")){
            displayNewMessage(username, chatTextField.getText());
            chatTextField.setText("");
            chatClient.sendChatMessage(destinationUser, textFieldMessage);
        }
    }
}