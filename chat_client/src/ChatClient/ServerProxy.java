package ChatClient;

import Utility.CommunicationService;
import Utility.Message;
import Utility.MessageContent;
import static Utility.Message.MessageIdentifier.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A mediator for communications with a Chat Server. Communicates with the
 * Chat Server by sending serialized messages through a CommunicationService.
 * A null reference of CommunicationService will be passed if no Chat Server is
 * available for connection. If a null CommunicationService is detected, error
 * messages will be printed to the console.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class ServerProxy {

    private int serverPort;
    private String serverIPAddress;
    private boolean isNormalMode;

    /**
     * Constructor for a ServerProxy. The ServerProxy needs port and IP information in order to create
     * a new CommunicationService for each message sent to the actual server across the network. If the
     * ServerProxy is alerted that the ChatClient has been launched in something other than normal mode,
     * some message will be handled differently.
     * @param serverPort Port number of the server the client is reaching out to.
     * @param serverIPAddress IP address of the server the client is reaching out to.
     * @param isNormalMode true if the Chat Client was launched in normal mode.
     */
    public ServerProxy(int serverPort, String serverIPAddress, boolean isNormalMode){
        this.serverPort = serverPort;
        this.serverIPAddress = serverIPAddress;
        this.isNormalMode = isNormalMode;
    }

    /**
     * Sends a CONNECTION_REQ message to the Server with the username of the ChatClient trying to connect. If the
     * server is unreachable, the returnMessage will return null, an error message will print, and the program will
     * be exited. If the server receives the request, the server will either respond with an OKAY (meaning the
     * connection was successful) or a USERNAME_IN_USE (meaning the username provided is already logged as an
     * online user on the server side).
     * @param userName the username of the Chat Client trying to connect
     * @return The message identifier returned by the server (OKAY or USERNAME_IN_USE)
     */
    public Message.MessageIdentifier requestToConnect(String userName){
        MessageContent messageContent = new MessageContent();
        messageContent.setSourceUser(userName);

        Message message = new Message(CONNECTION_REQ, messageContent);

        Message returnMessage = sendMsg(message);

        if(returnMessage == null){
            System.out.println("Unable to connect to server. Make sure you are entering the correct port and IP, " +
                    "and that the server is online.");
            System.exit(1);
        }

        return returnMessage.getMessageIdentifier();
    }

    /**
     * Sends a chat message from a client to the server. If the server sees the destination user is online when the
     * servers receives the message, it will respond with an OKAY, else it will respond with a MSG_PENDING (meaning
     * the message will sit in a queue on the server side until that user comes online, at which point the message
     * will be sent to that user once that user's ChatUpdater requests it).
     * @param sourceUser The username of the client sending the message.
     * @param destinationUser The username of the client the message is addressed to.
     * @param chatContents The actual chat message.
     * @return The message identifier returned by the server (OKAY or MSG_PENDING)
     */
    public Message.MessageIdentifier sendChatMessage(String sourceUser, String destinationUser, String chatContents){
        MessageContent messageContent = new MessageContent();
        messageContent.setSourceUser(sourceUser);
        messageContent.setDestinationUser(destinationUser);
        messageContent.setChatContent(chatContents);
        messageContent.setTimestampSent(getCurrentTimestamp());

        Message message = new Message(CHAT_MSG, messageContent);

        Message returnMessage = sendMsg(message);

        if(returnMessage != null){
            return returnMessage.getMessageIdentifier();
        }

        return null;
    }

    /**
     * Requests the next queued message from this client's message queue on the server. The queue holds any messages
     * addressed to this client that have been sent from other clients to the server. Null is returned if there is
     * no new messages, or there is trouble contacting the server.
     * @param sourceUser This client's username
     * @return the MessageContent of the most recently popped message, or null if there were no new messages.
     */
    public MessageContent requestNextQueuedMessage(String sourceUser){
        MessageContent messageContent = new MessageContent();
        messageContent.setSourceUser(sourceUser);

        Message message = new Message(GET_NEXT_QUEUED_MSG, messageContent);

        Message returnMessage = sendMsg(message);

        if(returnMessage != null){
            return returnMessage.getMessageContent();
        }

        return null;
    }

    /**
     * Let's the server know that this client is disconnecting from the server (closing the client-side application).
     * This provides the server the data it needs to maintain it's list of active users.
     * @param sourceUser the username of the disconnecting client
     */
    public void disconnect(String sourceUser){
        MessageContent messageContent = new MessageContent();
        messageContent.setSourceUser(sourceUser);

        Message message = new Message(DISCONNECT, messageContent);

        sendMsg(message);
    }

    /**
     * "Stamps" a MessageContent with the current time, then sends a message to the server to let it know that
     * the message has been received by this client.
     * @param messageContent The entire message that was received by this client, modified with "received timestamp
     *                       data".
     */
    public void sendTimestampReceivedData(MessageContent messageContent){
        messageContent.setTimestampReceived(getCurrentTimestamp());
        sendMsg(new Message(TIMESTAMP_DATA, messageContent));
    }

    /**
     * @return the current time as in the string format HH MM SS LLLL, where H is hours, M is minutes, S is
     * seconds, and L is milliseconds.
     */
    private String getCurrentTimestamp(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH.mm.ss.SSS");
        return new SimpleDateFormat("HH.mm.ss.SSS").format(new Date());
    }

    /**
     * The underlying helper method that allows all public methods in this class to send messages to the actual
     * server. Attempts to create a communication service, and returns the message sent back from the server
     * once the server processes the message.
     * @param message Entire message to be sent across the network, which contains both the MessageIdentifier and the
     *                MessageContent.
     * @return The return message from the server after the server processes the message sent. Returns null if there
     * was an issue creating a communication server.
     */
    private Message sendMsg(Message message){
        Message returnMessage = null;
        CommunicationService cs = createCommunicationService();

        if(cs != null){
            returnMessage = cs.sendMessage(message);
        }

        return returnMessage;
    }

    /**
     * Attempts to create a communication service that connects with the server. If the ChatClient was launched
     * in a mode other than normal mode, the program will exit immediately if it fails to communicate with the
     * server.
     * @return The communication service object created, or null if there was a problem communicating with the server.
     */
    private CommunicationService createCommunicationService(){
        CommunicationService cs = null;

        try{
            cs = new CommunicationService(serverIPAddress, serverPort);
        }catch(IOException e){
            //Kill the program if in an automated mode (sender/receiver) can't reach the server.
            if(!isNormalMode){
                System.out.println("Problem communicating with the server.");
                System.exit(1);
            }
        }

        return cs;
    }
}
