package ChatClient;

import Utility.Message;
import Utility.MessageContent;

/**
 * ChatClient will represent the chat user and will handle the
 * initialization of the ChatClient, will be unable to start if
 * correct fields are not provided. Uses a ServerProxy to send
 * messages to the server between different chat users. Will handle
 * different types of modes that the ChatClient can be launched as.
 *
 * If ChatClient is in normal mode, It will start the gui for the
 * user.
 * If It's in Sender / Receiver mode, the gui will not start and
 * output will directed towards the terminal.
 *
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class ChatClient {

    private Mode mode;
    private String receiverUsername;
    private String username;
    private ServerProxy serverProxy;
    private ChatDisplay chatDisplay;

    private int num_msgs_received = 0;
    private final int NUM_MSGS_TO_BE_SENT_IN_ANALYSIS_MODE = 3;

    private enum Mode {NORMAL, RECEIVER, SENDER};

    /**
     * Will perform proper setup depending on how the ChatClient was started,
     * given different modes it will setup accordingly. Will attempt to initially
     * connect to the server with a request message, if it fail the ChatClient
     * will exit.
     * @param serverPortNumber port of the server to connect to.
     * @param serverIPAddress ip of the server to connect to.
     * @param clientUsername name of the client.
     * @param mode mode that ChatClient was launched in.
     * @param receiverUsername target client for Sender mode.
     */
    public ChatClient(int serverPortNumber, String serverIPAddress,
                      String clientUsername, Mode mode, String receiverUsername) {

        this.mode = mode;
        this.receiverUsername = receiverUsername;
        username = clientUsername;

        serverProxy = new ServerProxy(serverPortNumber, serverIPAddress, isNormalMode());

        Message.MessageIdentifier connectionStatus = serverProxy.requestToConnect(clientUsername);

        if(connectionStatus.equals(Message.MessageIdentifier.CONNECTION_ACCEPT)){
            System.out.println("Connection Successful");

            if(isNormalMode()){
                chatDisplay = new ChatDisplay(this, username);
            }

            ChatUpdater chatUpdater = new ChatUpdater(this);
            Thread thread = new Thread(chatUpdater);
            thread.start();

            if(mode.equals(Mode.SENDER)){
                startSenderMode();
            }

        }else if(connectionStatus.equals(Message.MessageIdentifier.USERNAME_IN_USE)){
            System.out.println("Connection to Server Declined Due To Duplicate Username! Relaunch the program and" +
                    " enter a different name.");
            System.exit(1);
        }
    }

    /**
     * Disconnects the chatClient using the ServerProxy.
     */
    public void disconnectClient(){
        serverProxy.disconnect(username);
    }

    /**
     * Gets the next pending chat message that is stored at the server. Uses ServerProxy to send
     * the request message to get the next queued message. Additionaly, it will send time stamp of
     * the time that the message will be sent.
     */
    public void getNextQueuedChatMessage(){
        MessageContent messageContent = serverProxy.requestNextQueuedMessage(username);

        if(messageContent != null){
            serverProxy.sendTimestampReceivedData(messageContent);
            processChatMessage(messageContent);
        }
    }

    /**
     * Sends a message to the given destination user and chat contents.
     * Will display a failed attempt to send a message to the user on the gui.
     * @param destinationUser
     * @param chatContents
     */
    public void sendChatMessage(String destinationUser, String chatContents){

        Message.MessageIdentifier sendMessageStatus =
                serverProxy.sendChatMessage(username, destinationUser, chatContents);

        if(isNormalMode()){
            if(sendMessageStatus == null){
                chatDisplay.displayServerUnresponsiveMessage();
            }else if(sendMessageStatus.equals(Message.MessageIdentifier.MSG_PENDING)){
                chatDisplay.displayMessagePending(destinationUser);
            }
        }
    }

    /**
     * Determines whether the current mode is normal mode.
     */
    private boolean isNormalMode(){
        return mode.equals(Mode.NORMAL);
    }

    /**
     * Will start sender mode by sending a defined number of messages to the
     * receiver username for analysis mode. Will exit the ChatClient when
     * It's finished sending all of it's messages.
     */
    private void startSenderMode(){
        for(int i = 0; i < NUM_MSGS_TO_BE_SENT_IN_ANALYSIS_MODE; i++){
            sendChatMessage(receiverUsername, Integer.toString(i+1));
        }
        System.out.println("Sent all messages to Receiver: " + receiverUsername);
        serverProxy.disconnect(username);
        System.exit(0);
    }

    /**
     * Will check if all the messages are received from it's sender pair,
     * if so then ChatClient will exit.
     */
    private void startReceiverMode() {
        num_msgs_received++;
        if (num_msgs_received >= NUM_MSGS_TO_BE_SENT_IN_ANALYSIS_MODE) {
            System.out.println("Receiver User: " + username + " received all messages");
            disconnectClient();
            System.exit(0);
        }
    }

    /**
     * Will process a chat message depending on the type of mode that the
     * ChatClient is in. If it's in normal mode then it will display that message
     * to the users gui. If it's in receiver mode then it will check whether all
     * messages are recieved from its sender pair.
     * @param messageContent contents fo the chat message received from ServerProxy.
     */
    private void processChatMessage(MessageContent messageContent){
        if(mode.equals(Mode.NORMAL)){
            displayMessage(messageContent);
        }else if(mode.equals(Mode.RECEIVER)){
            startReceiverMode();
        }
    }

    /**
     * Will display the chat contents contained in messageContent to the gui.
     * @param messageContent
     */
    private void displayMessage(MessageContent messageContent){
        chatDisplay.displayNewMessage(messageContent.getSourceUser(), messageContent.getChatContent());
    }

    /**
     * Entry point for the ChatClient, accepts different types of arguments
     * to initialize the ChatClient. Will expect a server port and ip and username.
     * Additionally, if different modes are used an additional argument will be needed.
     * @param args
     */
    public static void main (String[] args){
        //args: client port client IP client username server port server IP
        String clientUsername = "username";
        int serverPortNumber = 1234;
        String serverIPAddress = "localhost";
        Mode mode = Mode.NORMAL;
        String receiverUsername = "receiver username";

        if(args.length >= 3 && args.length <= 5) {
            serverPortNumber = Integer.parseInt(args[0]);
            serverIPAddress = args[1];
            clientUsername = args[2];

            if(args.length == 4 && args[3].equals("Receiver")){
                mode = Mode.RECEIVER;
            }else if(args.length == 5 && args[3].equals("Sender")){
                mode = Mode.SENDER;
                receiverUsername = args[4];
            }
        }

        ChatClient chatClient = new ChatClient(serverPortNumber,
                serverIPAddress, clientUsername, mode, receiverUsername);
    }
}
